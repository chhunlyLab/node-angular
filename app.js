require('dotenv').config({path: __dirname + '/.env'});
const express = require('express');
const app = express();
const index = 'index.html';
const webDir = __dirname + '/angular-user/dist/angular-user';
const addressRoutes =  require('./api/routes/address');

app.use(express.static(webDir));
app.set('view engine', 'pug');
app.get('/application', (req, res) => {
    res.sendFile(index, {root: webDir});
});
app.use('/api/address', addressRoutes);
// app.get('/user', (req, res) => {
//     res.sendFile(index, {root: webDir});
// });
// app.get('/role', (req, res) => {
//     res.sendFile(index, {root: webDir});
// });

module.exports = app;
