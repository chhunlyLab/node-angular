import { Component, OnInit } from '@angular/core';
import {AddressService} from './_services/address.services';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'angular-user';
  data: any;
  constructor(
    private addressService: AddressService
    ){

  }
  ngOnInit(): void {
    this.list();
  }
  list = () => {
    this.addressService.list().toPromise().then(res => {
      console.log(res);
      this.data = res;
    })
  }
  onStatusClick(){
    alert('hello world')
  }
}
