import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class AddressService{
    constructor(private http: HttpClient){
        
    }
    list = () => {
        return this.http.get('/api/address');
    }
}