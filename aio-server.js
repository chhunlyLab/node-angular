require('dotenv').config({path: __dirname + '/.env'});
const http = require('http');
const app = require('./app');
const reload = require('reload');
const port = process.env.PORT;

const server = http.createServer(app);
reload(app).then(function (reloadReturned) {
    // reloadReturned is documented in the returns API in the README
  
    // Reload started, start web server
    server.listen(port, function () {
        console.log('Web server listening on port ' + port)
    })
}).catch(function (err) {
    console.error('Reload could not start, could not start server/sample app', err)
});